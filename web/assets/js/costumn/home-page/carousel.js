$(document).ready(function () {

    var $window = $(window);

    $window.resize(function resize() {
        if ($window.width() < 1295) {

            $('#submenu').removeClass('profile-subnav-show');
            $('#click').removeClass('cloff');
        }
    });

    $(".a-login-list").click(function () {
        $('#submenu').addClass('profile-subnav-show');
        $('#click').toggleClass('clon cloff', 1000);
        event.preventDefault();
    });


    $('li.sidebar-li-has-ul').click(function () {
        $(this).children('.sideabar-sub-ul').slideToggle(500);
        $(this).toggleClass('active inactive');
        $(this).find('i#angle-down').toggleClass('fa-angle-down fa-angle-up', 1000);
        event.preventDefault();
    });

    $(".fa-bars").click(function () {
        $('#test').toggleClass('sidebar-display-show sidebar-display-none', 1000);
    });



// OWL CAROUSEL
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        dots: true,
        nav: true,
        dotsData: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        dotsContainer: '#carousel-custom-dots',
        responsiveClass: true,
        navText: [
            "<i class='fa fa-angle-left '></i>",
            "<i class='fa fa-angle-right'></i>"
        ],

        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: true
            },
            1000: {
                items: 1,
                nav: true,
                loop: true
            }
        }
    });

});


