<?php

namespace AdminBundle\Controller;

use GuzzleHttp\Psr7\Request;
use Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:body:home-page/home-page.html.twig', array(
            'name' => 'Carolina',
            'navlist' => array('Home', 'Shop', 'Portofolio', 'Fashion'),
            'related-articles-page' => '9 Comments',
            'myphoto' => 'assets/images/about.png',
            'title' => 'Dashboard Home',
            'logoImg' => 'assets/images/logoo.png',
            'image' => '../assets/images/car3.jpg',
            'imagePopular' => '../assets/images/car10.jpg',
            'imageCalendar' => '../assets/images/calendar3.jpg',
            'imageWeather' => '../assets/images/weather.jpg',
            'imageToDO' => '../assets/images/car7.jpg',

            'aboutMe' => array(
                'title' => 'Mother and English Teacher',
                'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantiumSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium.Sed ut perspiciatis unde omnis iste natus 
                error sit voluptatem accusantium doloremque laudantium.. '
            ),
            'categories' => array('Fashion', 'Fashion', 'Fashion', 'Fashion'),
            'tags' => array('Fashion', 'Makeup', 'Fashion', 'Love', 'Love', 'Love'),
            'comments' => array(
                array('imgPath' => 'assets/images/about.png',
                    'name' => 'Petru Bujac',
                    'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur'),
                array('imgPath' => 'assets/images/about.png',
                    'name' => 'Petru Bujac',
                    'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur')),
        ));
    }


    /**
     * @Route("/articles")
     */
    public function articlesAction()
    {


        return $this->render(
            'AdminBundle:body:articles-page/index.html.twig',
            array(
                'name' => 'Petru',
                'navlist' => array('Home', 'Shop', 'Portofolio', 'Fashion'),
                'related-articles-page' => '9 Comments',
                'myphoto' => 'assets/images/about.png',
                'title' => 'Dashboard Home',
                'logoImg' => 'assets/images/logoo.png',
                'image' => '../assets/images/car3.jpg',
                'imagePopular' => '../assets/images/car10.jpg',
                'imageCalendar' => '../assets/images/calendar3.jpg',
                'imageWeather' => '../assets/images/weather.jpg',
                'imageToDO' => '../assets/images/car7.jpg',

                'aboutMe' => array(
                    'title' => 'Mother and English Teacher',
                    'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantiumSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium.Sed ut perspiciatis unde omnis iste natus 
                error sit voluptatem accusantium doloremque laudantium.. '
                ),
                'categories' => array('Fashion', 'Fashion', 'Fashion', 'Fashion'),
                'tags' => array('Fashion', 'Makeup', 'Fashion', 'Love', 'Love', 'Love'),
                'comments' => array(
                    array('imgPath' => 'assets/images/about.png',
                        'name' => 'Petru Bujac',
                        'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur'),
                    array('imgPath' => 'assets/images/about.png',
                        'name' => 'Petru Bujac',
                        'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur')),
            )
        );
    }


    /**
     * @Route("/articles/edit")
     */
    public function articlesEditAction()
    {
        return $this->render('AdminBundle:body:edit-article-page/index.html.twig', array(
            'name' => 'Petru',
            'navlist' => array('Home', 'Shop', 'Portofolio', 'Fashion'),
            'related-articles-page' => '9 Comments',
            'myphoto' => '../assets/images/about.png',
            'title' => 'Dashboard Home',
            'logoImg' => '../assets/images/logoo.png',
            'image' => '../assets/images/car3.jpg',
            'imagePopular' => '../../assets/images/car10.jpg',
            'imageCalendar' => '../../assets/images/calendar3.jpg',
            'imageWeather' => '../assets/images/weather.jpg',
            'imageToDO' => '../assets/images/car7.jpg',

            'aboutMe' => array(
                'title' => 'Mother and English Teacher',
                'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantiumSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium.Sed ut perspiciatis unde omnis iste natus 
                error sit voluptatem accusantium doloremque laudantium.. '
            ),
            'categories' => array('Fashion', 'Fashion', 'Fashion', 'Fashion'),
            'tags' => array('Fashion', 'Makeup', 'Fashion', 'Love', 'Love', 'Love'),
            'comments' => array(
                array('imgPath' => 'assets/images/about.png',
                    'name' => 'Petru Bujac',
                    'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur'),
                array('imgPath' => 'assets/images/about.png',
                    'name' => 'Petru Bujac',
                    'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur')),
        ));
    }

}
