<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $articles = array();

        $topArticles = array();


        for ($i = 0; $i < 6; $i++) {
            $article = new Article();
            $article->setTitle('Where the wild things are review');
            $article->setCategory('Paragraph');
            $article->setDatePosted(new \DateTime());
            $article->setDescription('Lommodo ligula eget dolor. Aenean massa. Cum sociis que penatibus et 
        magnis dis parturient montes lorem, nascetur ridiculus mus. Donec quam felis, ultricies nec,...');

            if ($i % 2 == 0)
                $article->setMainImgPath('assets/images/leaf.jpg');
            else
                $article->setMainImgPath('assets/images/yellow.jpg');

            $articles[] = $article;//,$article,$article);

        }
        for ($i = 0; $i < 4; $i++) {
            $article = new Article();
            $article->setTitle('Where the wild things are review');
            $article->setCategory('Paragraph');
            $article->setDatePosted(new \DateTime());
            $article->setDescription('Lommodo ligula eget dolor. Aenean massa. Cum sociis que penatibus et 
        magnis dis parturient montes lorem, nascetur ridiculus mus. Donec quam felis, ultricies nec,...');

            if ($i % 2 == 0)
                $article->setMainImgPath('assets/images/cactus.jpg');
            else
                $article->setMainImgPath('assets/images/yellow.jpg');

            $topArticles[] = $article;//,$article,$article);

        }


        return $this->render('home-page/index.html.twig', array(
            'name' => 'Petru',
            'navlist' => array('Home', 'Shop', 'Portofolio', 'Fashion'),
            'images' => array(
                'assets/images/car.jpg',
                'assets/images/car.jpg',
                'assets/images/car.jpg',
                'assets/images/car.jpg',
                ),
            'comments' => '9 Comments',
            'myphoto' => 'assets/images/me.jpg',
            'articles-page' => $articles,
            'articles' => $articles,
            'topArticles' => $topArticles,
            'aboutMe' => array(
                'title' => 'Mother and English Teacher',
                'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantiumSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium.Sed ut perspiciatis unde omnis iste natus 
                error sit voluptatem accusantium doloremque laudantium.. '
            ),
            'categories' => array('Fashion', 'Fashion', 'Fashion', 'Fashion'),
        ));
    }


    /**
     * @Route("/article", name="article")
     */
    public function articleIndex(Request $request)
    {


        $topArticles = array();
        for ($i = 0; $i < 4; $i++) {
            $article = new Article();
            $article->setTitle('Where the wild things are review');
            $article->setCategory('Paragraph');
            $article->setDatePosted(new \DateTime());
            $article->setDescription('Lommodo ligula eget dolor. Aenean massa. Cum sociis que penatibus et 
        magnis dis parturient montes lorem, nascetur ridiculus mus. Donec quam felis, ultricies nec,...');

            if ($i % 2 == 0)
                $article->setMainImgPath('assets/images/cactus.jpg');
            else
                $article->setMainImgPath('assets/images/yellow.jpg');

            $topArticles[] = $article;//,$article,$article);

        }


        $topArticles = array();
        for ($i = 0; $i < 4; $i++) {
            $article = new Article();
            $article->setTitle('Where the wild things are review');
            $article->setCategory('Paragraph');
            $article->setDatePosted(new \DateTime());
            $article->setDescription('Lommodo ligula eget dolor. Aenean massa. Cum sociis que penatibus et 
        magnis dis parturient montes lorem, nascetur ridiculus mus. Donec quam felis, ultricies nec,...');

            if ($i % 2 == 0)
                $article->setMainImgPath('assets/images/cactus.jpg');
            else
                $article->setMainImgPath('assets/images/yellow.jpg');

            $topArticles[] = $article;//,$article,$article);

        }


        $alternativesArticles = array();
        for ($i = 0; $i < 3; $i++) {
            $article = new Article();
            $article->setTitle('Where the wild things are review');
            $article->setCategory('Paragraph');
            $article->setDatePosted(new \DateTime());
            $article->setDescription('Lommodo ligula eget dolor. Aenean massa. Cum sociis que penatibus et 
        magnis dis parturient montes lorem, nascetur ridiculus mus. Donec quam felis, ultricies nec,...');

            $article->setMainImgPath('assets/images/minimal.jpg');

            $alternativesArticles[] = $article;

        }


        $article = new Article();
        $article->setTitle("Where is the" . " <strong><em>Inspiration</em></strong>");
        $article->setCategory('Paragraph');
        $article->setDatePosted(new \DateTime());
        $article->setDescription('
    <p>Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur suscipiantur
        id. Ex eam verear civibus qualisque.Qui id docendi corpora, per te possim evertitur persecuti. Cu sed aliquid fastidii salutatus. Duo an vidit liber, elit vivendum qui ei, in sanctus graecis tacimates duo. Vim et docendi recteque. Eros ipsum ea
        nam.</p>
 <img src="assets/images/yellow.jpg"/>
        
            <p> Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur
                suscipiantur id. Ex eam verear civibus qualisque.Qui id docendi corpora, per te possim evertitur persecuti. Cu sed aliquid fastidii salutatus. Duo an vidit liber, elit vivendum qui ei, in sanctus graecis tacimates duo. Vim et docendi recteque.
                Eros ipsum ea nam.ius sumo brute persius, euismod ocurreret incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur suscipiantur id. Ex eam verear civibus qualisque.Qui id docendi
                corpora, per te possim evertitur persecuti. Cu sed aliquid fastidii salutatus. Duo an vidit liber, elit vivendum qui ei, in sanctus graecis tacimates duo. Vim et docendi recteque. Eros ipsum ea nam.</p>
       
    <img src="assets/images/latte.jpg" />

            <p class="article-text-non-first-letter"> Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur
                suscipiantur id. Ex eam verear civibus qualisque.Qui id docendi corpora, per te possim evertitur persecuti. Cu sed aliquid fastidii salutatus. Duo an vidit liber, elit vivendum qui ei, in sanctus graecis tacimates duo. Vim et docendi recteque.
                Eros ipsum ea nam.ius sumo brute persius, euismod ocurreret incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur suscipiantur id. Ex eam verear civibus qualisque.Qui id docendi
                corpora, per te possim evertitur persecuti. Cu sed aliquid fastidii salutatus. Duo an vidit liber, elit vivendum qui ei, in sanctus graecis tacimates duo. Vim et docendi recteque. Eros ipsum ea nam.</p>');
        $article->setMainImgPath('assets/images/leaf.jpg');


        return $this->render('article-page/index.html.twig', array(
            'name' => 'Petru',
            'navlist' => array('Home', 'Shop', 'Portofolio', 'Fashion'),
            'related-articles-page' => '9 Comments',
            'myphoto' => 'assets/images/me.jpg',
            'article' => $article,
            'articles' => $article,
            'topArticles' => $topArticles,
            'aboutMe' => array(
                'title' => 'Mother and English Teacher',
                'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantiumSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium.Sed ut perspiciatis unde omnis iste natus 
                error sit voluptatem accusantium doloremque laudantium.. '
            ),
            'categories' => array('Fashion', 'Fashion', 'Fashion', 'Fashion'),
            'tags' => array('Fashion', 'Makeup', 'Fashion', 'Love', 'Love', 'Love'),
            'comments' => array(
                array('imgPath' => 'assets/images/about.png',
                'name' => 'Petru Bujac',
                'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur'),
                array('imgPath' => 'assets/images/about.png',
                    'name' => 'Petru Bujac',
                    'text' => 'Lorem ipsum dolor sit amet, eu euismod graecis constituam eum, ex possit dolorem eam. Eu ius sumo brute persius, euismod ocurreret
                 incorrupte ne vel. Id sed homero constituto, atqui graeco eam ex. Minim volumus ne sed, sea deserunt evertitur')),
            'alternativeArticles' => $alternativesArticles
        ));
    }


    /**
     * @Route("/login", name="login")
     */
    public function logInIndex(Request $request){


        return $this->render('login-page/index.html.twig', array(
        ));
    }
}
