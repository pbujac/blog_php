Blog Template 
===========
A blog website

## Built With

* [PHP](http://php.net) - The programming language used
* [Symfony](https://symfony.com) -  The web framework used
* [Sass](http://sass-lang.com) - CSS Style preprocessor


## Run project
Make sure you have installed PHP > 5.6.32, Composer and Symfony


 Open project and run in terminal:
```
composer install
php bin/console server:run
```
## Screenshots

### Main page
![Alt text](screenshots/img1.png?raw=true "Main Page")

### Article page
![Alt text](screenshots/img2.png?raw=true "Article Page")

### Admin Dashboard
![Alt text](screenshots/img3.png?raw=true "Dashboard Page")

### Admin List articles
![Alt text](screenshots/img4.png?raw=true "List articles Page")

### Responsive
![Alt text](screenshots/img5.png?raw=true "Responsive Pages")
